# Jestify

## INSTALL

```
git clone https://framagit.org/adimascio/jestify
cd jestify
npm install
```

## USAGE

```
node jestify.js /path/to/root/dir/to/convert
```

